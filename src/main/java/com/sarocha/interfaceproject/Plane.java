/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.interfaceproject;

/**
 *
 * @author Sarocha
 */
public class Plane extends Vehicle implements Flyable, Runable {

    private String engine;

    public Plane(String engine) {
        super(engine);
        this.engine = engine;
    }

    @Override
    public void startEngine() {
        System.out.println("Plane " + engine + " : start engine");
    }

    @Override
    public void stopEngine() {
        System.out.println("Plane " + engine + " : stop engine");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Plane " + engine + " : raise speed");
    }

    @Override
    public void applyBreak() {
        System.out.println("Plane " + engine + " : apply break");
    }

    @Override
    public void fly() {
        System.out.println("Plane " + engine + " : fly");
    }

    @Override
    public void run() {
        System.out.println("Plane " + engine + " : run");
    }

}
