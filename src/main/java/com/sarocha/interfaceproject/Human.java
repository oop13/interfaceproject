/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.interfaceproject;

/**
 *
 * @author Sarocha
 */
public class Human extends LandAnimal {

    private String nickname;

    public Human(String nickname) {
        super("Human", 2);
        this.nickname = nickname;
    }

    @Override
    public void eat() {
        System.out.println("Human : " + nickname + " eat");
    }

    @Override
    public void speak() {
        System.out.println("Human : " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Human : " + nickname + " sleep");
    }

    @Override
    public void run() {
        System.out.println("Human : " + nickname + " run");
    }

}
