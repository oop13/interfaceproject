/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarocha.interfaceproject;

/**
 *
 * @author Sarocha
 */
public class TestFlyable {

    public static void main(String[] args) {
        Bat bat1 = new Bat("Batman");
        Plane plane = new Plane("EnhypenI");
        bat1.fly();
        plane.fly();
        Dog dog1 = new Dog("Chaser");
        Cat cat1 = new Cat("Tom");
        Human h1 = new Human("Jesper");
        Human h2 = new Human("France");
        Car c1 = new Car("Mcqeen");
        space();
        Flyable[] flyables = {bat1, plane};
        for (Flyable f : flyables) {
            if (f instanceof Plane) {
                Plane p = (Plane) f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }
        space();
        Runable[] runables = {dog1, plane, cat1, h1, h2, c1};
        for (Runable r : runables) {
            if (r instanceof Car) {
                Car car = (Car) r;
                car.startEngine();
                car.run();
                car.raiseSpeed();
            }
            r.run();
        }

    }
    
    public static void space() {
        System.out.println();
    }
}
